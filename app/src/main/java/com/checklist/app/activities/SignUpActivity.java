package com.checklist.app.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.SignUpModel;
import com.checklist.app.utils.ChecklistPreferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editUserNameET)
    EditText editUserNameET;
    @BindView(R.id.rlBackIV)
    RelativeLayout rlBackIV;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.txtSignUpTV)
    TextView txtSignUpTV;
    @BindView(R.id.txtAlreadyHaveAccountTV)
    TextView txtAlreadyHaveAccountTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setStatusBarColor(mActivity);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.txtSignUpTV, R.id.rlBackIV, R.id.txtAlreadyHaveAccountTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSignUpTV:
                performSignUpClick();
                break;
            case R.id.rlBackIV:
                onBackPressed();
                break;
            case R.id.txtAlreadyHaveAccountTV:
                onBackPressed();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity, LoginActivity.class));
        finish();
    }

    private void performSignUpClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeSignUpApi();
            }
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("name", editUserNameET.getText().toString().trim());
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSignUpApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signUpRequest(mParam()).enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignUpModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    ChecklistPreferences.writeBoolean(mActivity, ChecklistPreferences.ISLOGIN, true);
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.USER_ID, mModel.getData().getId());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.EMAIL, mModel.getData().getEmail());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.USERNAME, mModel.getData().getUsername());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.MOBILE_NUMBER, mModel.getData().getMoblie_number());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.PASSWORD, response.body().getData().getPassword());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.PROFILE_PICTURE, response.body().getData().getProfile_pic());
                    Intent intent = new Intent(mActivity, QuestionTopicActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else if (response.body().getStatus().equalsIgnoreCase("0")) {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (editUserNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter__name));
            flag = false;
        } else if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;
        }
        return flag;
    }

}
