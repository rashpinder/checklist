package com.checklist.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.fragments.HomeFragment;
import com.checklist.app.fragments.ProfileFragment;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.LoginModel;
import com.checklist.app.utils.ChecklistPreferences;
import com.checklist.app.utils.Constants;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = HomeActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.homeLL)
    LinearLayout homeLL;
    @BindView(R.id.profileLL)
    LinearLayout profileLL;
    @BindView(R.id.imgHomeIV)
    ImageView imgHomeIV;
    @BindView(R.id.imgProIV)
    ImageView imgProIV;
    RequestOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setStatusBarColor(mActivity);
        options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        if (getIntent() != null) {

            LocalBroadcastManager.getInstance(HomeActivity.this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String status = intent.getStringExtra("Status");
                    assert status != null;
                    if (status.equalsIgnoreCase("editImage")) {
                        Glide.with(getApplicationContext())
                                .load(context).load(intent.getStringExtra("editImage"))
                                .apply(options)
                                .into(imgProIV);
                    }
                }
            }, new IntentFilter("changeImage"));
        }
        else {
            String img = ChecklistPreferences.readString(mActivity, ChecklistPreferences.PROFILE_PICTURE, null);
            Glide.with(getApplicationContext())
                    .load(img)
                    .apply(options)
                    .into(imgProIV);
        }
        performHomeClick();
    }

    @Override
    protected void onResume() {
        super.onResume();

        }

    @OnClick({R.id.homeLL, R.id.profileLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.homeLL:
                performHomeClick();
                break;
            case R.id.profileLL:
                performProfileClick();
                break;
        }
    }

    private void performProfileClick() {
        imgHomeIV.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_home));
        switchFragment(new ProfileFragment(), Constants.PROFILE_FRAGMENT, false, null);
        String img = ChecklistPreferences.readString(mActivity, ChecklistPreferences.PROFILE_PICTURE, null);
        Glide.with(getApplicationContext())
                .load(img)
                .apply(options)
                .into(imgProIV);
    }

    private void performHomeClick() {
        imgHomeIV.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_home_sel));
        String img = ChecklistPreferences.readString(mActivity, ChecklistPreferences.PROFILE_PICTURE, null);
        Glide.with(getApplicationContext())
                .load(img)
                .apply(options)
                .into(imgProIV);
        switchFragment(new HomeFragment(), Constants.HOME_FRAGMENT, false, null);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
       dismissProgressDialog();
    }
}