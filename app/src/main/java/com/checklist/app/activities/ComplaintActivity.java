package com.checklist.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.checklist.app.R;
import com.checklist.app.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComplaintActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ComplaintActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = ComplaintActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtAnsTV)
    TextView txtAnsTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    Boolean complaint;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint);
        setStatusBarColor(mActivity);
        ButterKnife.bind(this);
        getIntentData();
        if (complaint) {
            txtAnsTV.setText(R.string.congratulations);
        } else {
            txtAnsTV.setText(R.string.you_have_not_answered_yet);
        }
    }

    @OnClick({R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;}}

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    /*
      get intent data
       */
    private void getIntentData() {
        if (getIntent() != null) {
            complaint = getIntent().getBooleanExtra("completed", false);
        }

    }
}
