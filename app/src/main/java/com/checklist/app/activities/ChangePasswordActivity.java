package com.checklist.app.activities;

import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.ChangePassStatusModel;
import com.checklist.app.model.ChangePasswordModel;
import com.checklist.app.utils.ChecklistPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ChangePasswordActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ChangePasswordActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editOldPassET)
    EditText editOldPassET;
    @BindView(R.id.editNewPassET)
    EditText editNewPassET;
    @BindView(R.id.editConfirmPassET)
    EditText editConfirmPassET;
    @BindView(R.id.txtSaveTV)
    TextView txtSaveTV;
    ChangePasswordModel changePasswordModel;
    ApiInterface api = ApiClient.getApiClient().create(ApiInterface.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setStatusBarColor(mActivity);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.txtSaveTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSaveTV:
                performSaveClick();
                break;

        }
    }

    private void performSaveClick() {
        if (isValildate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                changePasswordModel = new ChangePasswordModel();
                changePasswordModel.setUser_id(ChecklistPreferences.readString(mActivity, ChecklistPreferences.USER_ID, null));
                changePasswordModel.setOld_password(editOldPassET.getText().toString());
                changePasswordModel.setNew_password(editConfirmPassET.getText().toString());
                executeChangePassApi(changePasswordModel);
            }
        }
    }

    private void executeChangePassApi(ChangePasswordModel changePasswordModel) {
        showProgressDialog(mActivity);
        api.addChangePassModel(changePasswordModel).enqueue(new Callback<ChangePassStatusModel>() {
            @Override
            public void onResponse(Call<ChangePassStatusModel> call, Response<ChangePassStatusModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (response.body().getStatus().equalsIgnoreCase("0")) {
                        dismissProgressDialog();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePassStatusModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    /*
    set up validations
     */
    public boolean isValildate() {
        boolean flag = true;
        if (editOldPassET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_current_password));
            flag = false;
        } else if (editNewPassET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_password));
            flag = false;
        } else if (editConfirmPassET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password));
            flag = false;
        } else if (!editNewPassET.getText().toString().trim().equals(editConfirmPassET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.password_mismatch));
            flag = false;
        } else if (editOldPassET.getText().toString().trim().length()<6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;}
        else if (editNewPassET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;
        } else if (editConfirmPassET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;
        } else if (editOldPassET.getText().toString().trim().equals(editNewPassET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.new_pass_and_old_pasword_different));
            flag = false;
        }
        return flag;
    }

}