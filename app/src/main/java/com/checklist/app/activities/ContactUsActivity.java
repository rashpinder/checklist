package com.checklist.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.ContactUsStatusModel;
import com.checklist.app.utils.ChecklistPreferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ContactUsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ContactUsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.imgProfileIV)
    ImageView imgProfileIV;
    @BindView(R.id.txtUsernameTV)
    TextView txtUsernameTV;
    @BindView(R.id.editContactET)
    EditText editContactET;
    @BindView(R.id.txtSendTV)
    TextView txtSendTV;
    ApiInterface api = ApiClient.getApiClient().create(ApiInterface.class);
    String mProfilePic;
    String mUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        setStatusBarColor(mActivity);
        setDataOnWidgets();
    }
    private void setDataOnWidgets() {
        mProfilePic=  ChecklistPreferences.readString(mActivity,ChecklistPreferences.PROFILE_PICTURE,null);
        mUserName=  ChecklistPreferences.readString(mActivity,ChecklistPreferences.USERNAME,null);
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(ContactUsActivity.this)
                .load(mProfilePic)
                .apply(options)
                .into(imgProfileIV);
        txtUsernameTV.setText(mUserName);

    }

    @OnClick({R.id.txtSendTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSendTV:
                performSendClick();
                break;

        }
    }
    private void performSendClick() {
        if (isValildate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeContactUsApi();
            }
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", ChecklistPreferences.readString(mActivity,ChecklistPreferences.USER_ID,null));
        mMap.put("message", editContactET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeContactUsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.contactUsRequest(mParam()).enqueue(new Callback<ContactUsStatusModel>() {
            @Override
            public void onResponse(Call<ContactUsStatusModel> call, Response<ContactUsStatusModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ContactUsStatusModel mModel = response.body();
                if (mModel.getStatus().equalsIgnoreCase("1")) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ContactUsStatusModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    public boolean isValildate()
    {
        boolean flag=true;
        if(editContactET.getText().toString().trim().equals(""))
        {
            showAlertDialog(mActivity,getString(R.string.enter_msg));
            flag=false;
        }
        return flag;
    }
    }
