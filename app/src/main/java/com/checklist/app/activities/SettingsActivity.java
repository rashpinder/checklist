package com.checklist.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.checklist.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SettingsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SettingsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtEditProfileTV)
    TextView txtEditProfileTV;
    @BindView(R.id.txtContactUsTV)
    TextView txtContactUsTV;
    @BindView(R.id.txtTermsTV)
    TextView txtTermsTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setStatusBarColor(mActivity);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.txtEditProfileTV, R.id.txtContactUsTV, R.id.txtTermsTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtEditProfileTV:
                performEditProfileClick();
                break;
            case R.id.txtContactUsTV:
                performContactUsClick();
                break;
            case R.id.txtTermsTV:
                performTermsClick();
                break;
        }
    }

    private void performTermsClick() {
        startActivity(new Intent(mActivity, TermsServiceActivity.class));
//        finish();
    }

    private void performContactUsClick() {
        startActivity(new Intent(mActivity, ContactUsActivity.class));
//        finish();
    }

    private void performEditProfileClick() {
        startActivity(new Intent(mActivity, EditProfileActivity.class));
//        finish();
    }
}
