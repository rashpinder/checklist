package com.checklist.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

import com.checklist.app.R;
import com.checklist.app.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.VISIBLE;

public class TermsServiceActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TermsServiceActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = TermsServiceActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.webViewWV)
    WebView webViewWV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_service);
        setStatusBarColor(mActivity);
        ButterKnife.bind(this);
        loadurl();

    }

    /*load web url*/
    private void loadurl() {
        showProgressDialog(mActivity);
        // To Show PDF IN WebView
        webViewWV.getSettings().setJavaScriptEnabled(true);
        webViewWV.getSettings().setBuiltInZoomControls(true);
        webViewWV.getSettings().setLoadWithOverviewMode(true);
        webViewWV.getSettings().setUseWideViewPort(true);
        webViewWV.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
             showProgressDialog(mActivity);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
               dismissProgressDialog();
            }
        });
        webViewWV.loadUrl(Constants.TERMS_SERVICES);

    }
}
