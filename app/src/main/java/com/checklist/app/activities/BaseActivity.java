package com.checklist.app.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.checklist.app.R;
import com.checklist.app.utils.ChecklistPreferences;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    /*
    * Get Class Name
    * */
    String TAG = BaseActivity.this.getClass().getSimpleName();
    /*
    * Initialize Activity
    * */
    Activity mActivity = BaseActivity.this;

    /*
    * Initialize Other Classes Objects...
    * */
    public Dialog progressDialog;

    /*
    * Get Is User Login
    * */
    public Boolean IsUserLogin(){
        return ChecklistPreferences.readBoolean(mActivity,ChecklistPreferences.ISLOGIN,false);
    }
    /*
    * Get User ID
    * */
    public String getUserID(){
        return ChecklistPreferences.readString(mActivity,ChecklistPreferences.USER_ID,"");
    }

    /*
    * Get User Email ID
    * */
    public String getUserEmailID(){
        return ChecklistPreferences.readString(mActivity,ChecklistPreferences.EMAIL,"");
    }

    /*
    * Get User Name
    * */
    public String getUserName(){
        return ChecklistPreferences.readString(mActivity,ChecklistPreferences.USERNAME,"");
    }
    /*
    * Get User Mobile Number
    * */
    public String getUserMobileNumber(){
        return ChecklistPreferences.readString(mActivity,ChecklistPreferences.MOBILE_NUMBER,"");
    }

    /*
    * Get User Profile Picture
    * */
    public String getUserProfilePicture(){
        return ChecklistPreferences.readString(mActivity,ChecklistPreferences.PROFILE_PICTURE,"");
    }

    /*
    * Get Activity Log
    * */
    public String getActivityFlag(){
        return ChecklistPreferences.readString(mActivity,ChecklistPreferences.ACTIVITY_FLAG,"");
    }


    /*
    * Set Theme & Status Bar Color
    * */
    public void setStatusBarColor(Activity mActivity){
        Window window = mActivity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(mActivity, R.color.colorBtnStart));
    }


    /*
     * Finish the activity
     * */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionSlideDownExit();
    }

    /*
     * To Start the New Activity
     * */
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionSlideUPEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    public void overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    public void overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down);
    }


    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();
    }



    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoad(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

    /*to switch between fragments*/
    public void switchFragment(final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainFL, fragment, Tag);
            if (addToStack)
                fragmentTransaction.addToBackStack(Tag);
            if (bundle != null)
                fragment.setArguments(bundle);
            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();
        }}

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    /*
     * Share Gmail Intent
     * */
    public void shareIntentGmail(Activity mActivity, String profileUrl) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Deal Breaker:");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out friend profile and share your view : " + profileUrl);
        mActivity.startActivity(Intent.createChooser(sharingIntent, "Choose One"));
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /*
     * Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialogFinish(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }


    public String formatException(Exception exception) {
        String formattedString = "Internal Error";
        Log.e("BaseActivity", " -- Error: " + exception.toString());
        Log.getStackTraceString(exception);

        String temp = exception.getMessage();

        if (temp != null && temp.length() > 0) {
            formattedString = temp.split("\\(")[0];
            if (temp != null && temp.length() > 0) {
                return formattedString;
            }
        }

        return formattedString;
    }


    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);
                    }
                }, mYear, mMonth, mDay);

//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 568025136000L);

        datePickerDialog.show();
    }


    public boolean isValidDateOfBirth(TextView mTextView) {
        boolean isDateValid = false;
        String dob = mTextView.getText().toString().trim();

        long mTimeInLong = getTimeInLong(dob);
        long difference = System.currentTimeMillis() - mTimeInLong;
        long dy = TimeUnit.MILLISECONDS.toDays(difference);

        final long years = dy / 365;

        isDateValid = years >= 18;
        Log.e(TAG, "isValidDateOfBirth: " + isDateValid);
        Log.e(TAG, "isValidDateOfBirth: " + years);

        return isDateValid;
    }


    private long getTimeInLong(String mDate) {
        long startDate = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(mDate);

            startDate = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return startDate;
    }


    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }


    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    public long calculateDateDifference(String strDateOfBirth) {
        Date startDate = null;
        Date endDate = null;


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");

        try {
            startDate = simpleDateFormat.parse(strDateOfBirth);
            endDate = simpleDateFormat.parse(getCurrentDate());

        } catch (ParseException e) {
            e.printStackTrace();
        }


        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

//        System.out.printf("%d days, %d hours, %d minutes, %d seconds%n",elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        long years = elapsedDays / 365;


        return years;
    }


    public String getCurrentDate() {
        Date d = new Date();
        CharSequence mCurrentDate = DateFormat.format("dd/mm/yyyy", d.getTime());
        return mCurrentDate.toString();
    }

}
