package com.checklist.app.activities;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.EditProfileStatusModel;
import com.checklist.app.model.ProfileModel;
import com.checklist.app.model.ProfileModelStatus;
import com.checklist.app.utils.ChecklistPreferences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = EditProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditProfileActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editUsernameET)
    EditText editUsernameET;
    @BindView(R.id.imgEditProfileIV)
    CircleImageView imgEditProfileIV;
    @BindView(R.id.txtEmailTV)
    TextView txtEmailTV;
    @BindView(R.id.txtChangePassTV)
    TextView txtChangePassTV;
    @BindView(R.id.editContactET)
    EditText editContactET;
    @BindView(R.id.txtSaveTV)
    TextView txtSaveTV;
    String mBase64Image="";
    ApiInterface api = ApiClient.getApiClient().create(ApiInterface.class);
    ProfileModel profileModel;
    RequestOptions options;

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setStatusBarColor(mActivity);
        getUserProfileDetails();
        ButterKnife.bind(this);

    }
    private void getUserProfileDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetUserDetailsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", ChecklistPreferences.readString(mActivity,ChecklistPreferences.USER_ID,null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetUserDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(mParams()).enqueue(new Callback<ProfileModelStatus>() {
            @Override
            public void onResponse(Call<ProfileModelStatus> call, Response<ProfileModelStatus> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ProfileModelStatus mGetDetailsModel = response.body();
                assert mGetDetailsModel != null;
                if (mGetDetailsModel.getStatus().equalsIgnoreCase("1")) {
                    String imageurl = mGetDetailsModel.getData().getProfile_pic();
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    if (imageurl!=null){
                        Glide.with(mActivity)
                                .load(imageurl)
                                .apply(options)
                                .into(imgEditProfileIV);}


                    else {
                        String img=ChecklistPreferences.readString(mActivity,ChecklistPreferences.PROFILE_PICTURE,null);
                        Glide.with(mActivity)
                                .load(img)
                                .apply(options)
                                .into(imgEditProfileIV);
                    }
                    if (imageurl!=null) {
                        Intent intent = new Intent("changeImage");
                        intent.putExtra("Status", "editImage");
                        intent.putExtra("editImage", imageurl);
                        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
                    }
                  editUsernameET.setText(response.body().getData().getUsername());
                    txtEmailTV.setText(response.body().getData().getEmail());
                    txtChangePassTV.setText(response.body().getData().getPassword());
                    editContactET.setText(response.body().getData().getMoblie_number());
                } else if (response.body().getStatus().equalsIgnoreCase("0")) {
                    showAlertDialog(mActivity, mGetDetailsModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModelStatus> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    @OnClick({R.id.txtSaveTV,R.id.imgEditProfileIV,R.id.txtChangePassTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSaveTV:
                performSaveClick();
                break;
            case R.id.imgEditProfileIV:
                performEditProfileImageClick();
                break;
            case R.id.txtChangePassTV:
                performChangePassClick();
                break;
        }}

    private void performChangePassClick() {
        startActivity(new Intent(mActivity, ChangePasswordActivity.class));
    }
    /* Camera Gallery functionality
     * */
    private void performEditProfileImageClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage, "Title", null);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .skipMemoryCache(true)
                            .dontAnimate()
                            .dontTransform();

                    if (path != null) {
                        Glide.with(mActivity).load(path)
                                .apply(options)
                                .into(imgEditProfileIV);
                    }
//                        imgEditProfileIV.setImageBitmap(selectedImage);
                    mBase64Image = encodeTobase64(selectedImage);

                    Log.e(TAG, "**Image Base 64**" + mBase64Image);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    /*
     * Set up validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editUsernameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter__name));
            flag = false;}
        else if (editContactET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_contact));
            flag = false;}
        return flag;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void performSaveClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                saveUserProfileDetailsApi();
            }
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("name", editUsernameET.getText().toString().trim());
        mMap.put("email", txtEmailTV.getText().toString().trim());
        mMap.put("moblie_number", editContactET.getText().toString().trim());
        mMap.put("profile_image", mBase64Image);
        mMap.put("id", ChecklistPreferences.readString(mActivity,ChecklistPreferences.USER_ID,null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void saveUserProfileDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.saveProfileDetailsRequest(mParam()).enqueue(new Callback<EditProfileStatusModel>() {
            @Override
            public void onResponse(Call<EditProfileStatusModel> call, Response<EditProfileStatusModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                EditProfileStatusModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equalsIgnoreCase("1")) {
                    showToast(mActivity, mModel.getMessage());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.USER_ID, mModel.getId());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.PROFILE_PICTURE, mModel.getUserDetails().getProfilePic());
                    finish();
                } else if (response.body().getStatus().equalsIgnoreCase("0")) {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<EditProfileStatusModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }}

