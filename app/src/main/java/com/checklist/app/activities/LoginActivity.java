package com.checklist.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.LoginModel;
import com.checklist.app.utils.ChecklistPreferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = LoginActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = LoginActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.rlBackIV)
    RelativeLayout imgBackIV;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.txtLoginTV)
    TextView txtLoginTV;
    @BindView(R.id.txtForgotPassTV)
    TextView txtForgotPassTV;
    @BindView(R.id.txtDontHaveAccountTV)
    TextView txtDontHaveAccountTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setStatusBarColor(mActivity);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.txtForgotPassTV, R.id.txtLoginTV, R.id.rlBackIV, R.id.txtDontHaveAccountTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtForgotPassTV:
                performForgotPassClick();
                break;
            case R.id.txtLoginTV:
                performLoginClick();
                break;
            case R.id.rlBackIV:
                onBackPressed();
                break;
            case R.id.txtDontHaveAccountTV:
                performDothaveAccountClick();
                break;
        }
    }

    private void performDothaveAccountClick() {
        startActivity(new Intent(mActivity, SignUpActivity.class));
        finish();
    }

    private void performForgotPassClick() {
        startActivity(new Intent(mActivity, ForgotPasswordActivity.class));
        finish();
    }


    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        return flag;
    }

    private void performLoginClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeLoginApi();
            }
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeLoginApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginRequest(mParam()).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                LoginModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    ChecklistPreferences.writeBoolean(mActivity, ChecklistPreferences.ISLOGIN, true);
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.USER_ID, mModel.getData().getId());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.EMAIL, mModel.getData().getEmail());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.USERNAME, mModel.getData().getUsername());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.MOBILE_NUMBER, mModel.getData().getMoblie_number());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.PASSWORD, response.body().getData().getPassword());
                    ChecklistPreferences.writeString(mActivity, ChecklistPreferences.PROFILE_PICTURE, response.body().getData().getProfile_pic());
                    if (response.body().getCompletion_status().equals(false)) {
                        Intent intent = new Intent(mActivity, QuestionTopicActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        startActivity(new Intent(mActivity, HomeActivity.class));
                        finish();
                    }
                } else if (response.body().getStatus().equalsIgnoreCase("0")) {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


}
