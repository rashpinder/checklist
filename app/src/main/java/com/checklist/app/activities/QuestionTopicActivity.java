package com.checklist.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.adapters.TopicAdapter;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.TopicsData;
import com.checklist.app.model.TopicsModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionTopicActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = QuestionTopicActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = QuestionTopicActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.mTopicsRV)
    RecyclerView mTopicsRV;
    @BindView(R.id.llNextHomeLL)
    LinearLayout llNextHomeLL;

    /*
     * Initialize Objects
     * */
    ArrayList<TopicsData> mTopicsArrayList = new ArrayList<>();
    TopicAdapter mTopicAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ques_topic);
        ButterKnife.bind(this);
        setStatusBarColor(mActivity);

    }


    @OnClick({R.id.llNextHomeLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llNextHomeLL:
                performHomeClick();
                break;
        }
    }

    /*
    perform home click
     */
    private void performHomeClick() {
        startActivity(new Intent(mActivity, HomeActivity.class));
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        getIntentData();
    }


    /*
    get intent data
     */
    private void getIntentData() {
        showTopicList();
    }

    /*
    show topic list
     */
    private void showTopicList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeTopicListApi();
        }
    }



    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeTopicListApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.questionTopicRequest(mParam()).enqueue(new Callback<TopicsModel>() {
            @Override
            public void onResponse(Call<TopicsModel> call, Response<TopicsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TopicsModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mTopicsArrayList = response.body().getData();
                    setTopicAdapter();

                    if (mModel.getOverallPending() == 0) {
                        llNextHomeLL.setVisibility(View.VISIBLE);
                    } else {
                        llNextHomeLL.setVisibility(View.GONE);
                    }
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }


            @Override
            public void onFailure(Call<TopicsModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    /*
    set topic adapter
     */
    private void setTopicAdapter() {
        mTopicAdapter = new TopicAdapter(mActivity, mTopicsArrayList);
        mTopicsRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mTopicsRV.setAdapter(mTopicAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}