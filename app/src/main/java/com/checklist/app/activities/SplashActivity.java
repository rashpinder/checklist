package com.checklist.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.QuestionModel;
import com.checklist.app.utils.Constants;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = SplashActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = SplashActivity.this;


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        setSplash();
    }

    /*
    set splash
     */
    private void setSplash() {
        if (IsUserLogin()) {
            getQuestionAttemptStatus();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(mActivity, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }, 2000);
        }
    }

    /*
    get attempted questions status
     */
    private void getQuestionAttemptStatus() {
        if (isNetworkAvailable(mActivity))
            executeApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeApi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.questionResumeRequest(mParam()).enqueue(new Callback<QuestionModel>() {
            @Override
            public void onResponse(Call<QuestionModel> call, Response<QuestionModel> response) {
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                QuestionModel mModel = response.body();
                setValidations(mModel);
            }

            @Override
            public void onFailure(Call<QuestionModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
    set up validations
     */
    private void setValidations(QuestionModel mModel) {
        if (mModel.getStatus() == 1) {
            if (mModel.getOverallPending() == 0) {
                Intent i = new Intent(mActivity, HomeActivity.class);
                startActivity(i);
                finish();
            } else {
                if (mModel.getPendingCountCurrTopic() == 0) {
                    Intent i = new Intent(mActivity, QuestionTopicActivity.class);
                    startActivity(i);
                    finish();
                } else if (mModel.getData().getId().equals("0") && mModel.getData().getTopicId().equals("0") && mModel.getData().getSubtopicId().equals("0")) {
                    Intent i = new Intent(mActivity, QuestionTopicActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(mActivity, QuestionsActivity.class);
                    if (mModel.getData().getSubtopicId() != null && mModel.getData().getSubtopicId().length() > 0)
                        i.putExtra(Constants.TOPIC_ID, "" + mModel.getData().getSubtopicId());
                    else
                        i.putExtra(Constants.TOPIC_ID, "" + mModel.getData().getTopicId());
                    i.putExtra(Constants.QUESTION_ID, "" + mModel.getData().getId());
                    startActivity(i);
                    finish();
                }
            }

        } else {
            Intent i = new Intent(mActivity, QuestionTopicActivity.class);
            startActivity(i);
            finish();
        }
    }
}
