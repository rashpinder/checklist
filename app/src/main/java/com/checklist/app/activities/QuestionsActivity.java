package com.checklist.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.adapters.QuestionsPagerAdapter;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.interfaces.QuestionsInterface;
import com.checklist.app.model.QuestionDataModel;
import com.checklist.app.model.QuestionModel;
import com.checklist.app.utils.ChecklistPreferences;
import com.checklist.app.utils.Constants;
import com.checklist.app.views.CustomViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = QuestionsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = QuestionsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtNextTV)
    TextView txtNextTV;
    @BindView(R.id.questionVP)
    CustomViewPager questionVP;

    /*
     *
     * Initialize...Objects
     * */
    QuestionsPagerAdapter mQuestionsPagerAdapter;
    int mPosition = 0;
    ArrayList<QuestionDataModel> mQustionsArrayList = new ArrayList<>();
    String mTopicID = "";
    String mQuestionID = "";
    String mAnswer = "";
    String mColor = "";
    String mSelColor = null;


    QuestionsInterface mQuestionsInterface = new QuestionsInterface() {
        @Override
        public void onClick(String topic_id, String question_id, String ans, String color) {
            mTopicID = topic_id;
            mQuestionID = question_id;
            mAnswer = ans;
            mColor = color;
            Log.e(TAG, "**Questions Selections**" + "Topic Id : " + mTopicID + "  Question ID : " + mQuestionID + " " + "Answer :" + mAnswer + "   Color : " + mColor);
        }
    };

    /*override method on create*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        setStatusBarColor(mActivity);
        ButterKnife.bind(this);
        getIntentData();
    }

    /*
    get intent
     */
    private void getIntentData() {
        if (getIntent() != null) {
            if (getIntent().getStringExtra(Constants.TOPIC_ID) != null){
                mTopicID = getIntent().getStringExtra(Constants.TOPIC_ID);
                mQuestionID = getIntent().getStringExtra(Constants.QUESTION_ID);
                if (getIntent().getStringExtra(Constants.SELECTED_COLOR) != null){
                    mSelColor = getIntent().getStringExtra(Constants.SELECTED_COLOR);
                    executeQAColorTopicAPi();
                }else{
                    getQuestionsApi();
                }
            }
            Log.e(TAG, "**Topic ID**" + mTopicID + "**Question ID**" + mQuestionID);
        }
    }

    /*
    execute color topic api
     */
    private void executeQAColorTopicAPi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetQColorApi();
        }
    }

    private void executeGetQColorApi() {
        mQustionsArrayList.clear();
        showProgressDialog(mActivity);
        ApiInterface api = ApiClient.getApiClient().create(ApiInterface.class);
        api.saveColorTopic(mParam()).enqueue(new Callback<QuestionModel>() {
            @Override
            public void onResponse(Call<QuestionModel> call, Response<QuestionModel> response) {
                dismissProgressDialog();
                QuestionModel mModel = response.body();
                mTopicID = "";
                mQuestionID = "";
                mAnswer = "";
                mColor = "";
                if (mModel.getStatus() == 1) {
                    if (mModel.getPendingCountCurrTopic() == 0) {
                        showNewAlertDialog(mActivity, getString(R.string.you_have));
                    } else if (mModel.getData().getId().equals("0") && mModel.getData().getTopicId().equals("0") && mModel.getData().getSubtopicId().equals("0")) {
                        showNewAlertDialog(mActivity, getString(R.string.you_have));
                    } else {
                        mQustionsArrayList.add(mModel.getData());
                        setViewPagerAdapter();
                    }
                } else if (response.body().getStatus() == 0) {
                    dismissProgressDialog();
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<QuestionModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     get questions api
    */
    private void getQuestionsApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetQApi();
        }
    }

    /*
     * Execute Questions Api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("topic_id", mTopicID);
        mMap.put("question_id", mQuestionID);
        mMap.put("answer", mAnswer);
        mMap.put("color", mColor);
        mMap.put("base_color", mSelColor);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetQApi() {
        mQustionsArrayList.clear();
        showProgressDialog(mActivity);
        ApiInterface api = ApiClient.getApiClient().create(ApiInterface.class);
        api.questionGetSubmit(mParam()).enqueue(new Callback<QuestionModel>() {
            @Override
            public void onResponse(Call<QuestionModel> call, Response<QuestionModel> response) {
                dismissProgressDialog();
                QuestionModel mModel = response.body();
                mTopicID = "";
                mQuestionID = "";
                mAnswer = "";
                mColor = "";
                if (mModel.getStatus() == 1) {
                    if (mModel.getPendingCountCurrTopic() == 0) {
                        showNewAlertDialog(mActivity, getString(R.string.you_have));
                    } else if (mModel.getData().getId().equals("0") && mModel.getData().getTopicId().equals("0") && mModel.getData().getSubtopicId().equals("0")) {
                        showNewAlertDialog(mActivity, getString(R.string.you_have));
                    } else {
                        mQustionsArrayList.add(mModel.getData());
                        setViewPagerAdapter();
                    }
                } else if (response.body().getStatus() == 0) {
                    dismissProgressDialog();
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<QuestionModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Set ViewPager Adapter
     * */
    private void setViewPagerAdapter() {
        if (mQustionsArrayList != null) {
            mQuestionsPagerAdapter = new QuestionsPagerAdapter(mActivity, mQustionsArrayList, mQuestionsInterface);
            questionVP.setAdapter(mQuestionsPagerAdapter);
            questionVP.setPagingEnabled(false);
        }
    }

    @OnClick({R.id.txtNextTV, R.id.rlBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtNextTV:
                performNextClick();
                break;
            case R.id.rlBackIV:
                onBackPressed();
                break;
        }
    }

    private void performNextClick() {
        if ((mAnswer.equals(""))&&(mSelColor==null)) {
            showAlertDialog(mActivity, getString(R.string.one_option));
        } else {
            updateViewPager();
        }
    }

    /*
    update viewpager
     */
    private void updateViewPager() {
        if (mSelColor != null) {
            executeQAColorTopicAPi();
        } else {
            executeGetQApi();
        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showNewAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (mSelColor != null && mSelColor.length() > 0){
                    finish();
                }else{
                    startActivity(new Intent(mActivity, QuestionTopicActivity.class));
                    finish();
                }

            }
        });
        alertDialog.show();
    }
}