package com.checklist.app.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.activities.LoginActivity;
import com.checklist.app.activities.SettingsActivity;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.ProfileModel;
import com.checklist.app.model.ProfileModelStatus;
import com.checklist.app.utils.ChecklistPreferences;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = ProfileFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = ProfileFragment.this;

    Unbinder unbinder;

    /*
     * Widgets
     * */
    CircleImageView imgProfileIV;
    ImageView imgSettingsIV;
    TextView txtUserNameTV;
    TextView txtEmailTV;
    TextView txtChangePassTV;
    TextView txtContactTV;
    TextView txtLogoutTV;
    ProfileModel profileModel;
    RequestOptions options;



    public ProfileFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        imgSettingsIV= v.findViewById(R.id.imgSettingsIV);
        txtUserNameTV=v.findViewById(R.id.txtUserNameTV);
        txtEmailTV=v.findViewById(R.id.txtEmailTV);
        txtChangePassTV=v.findViewById(R.id.txtChangePassTV);
        txtContactTV=v.findViewById(R.id.txtContactTV);
        txtLogoutTV=v.findViewById(R.id.txtLogoutTV);
        imgProfileIV=v.findViewById(R.id.imgProfileIV);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick({R.id.txtLogoutTV,R.id.imgSettingsIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtLogoutTV:
                performLogoutClick();
                break;
            case R.id.imgSettingsIV:
                performSettingsClick();
                break;
        }}

    private void performSettingsClick() {
        startActivity(new Intent(getActivity(), SettingsActivity.class));
    }

    private void performLogoutClick() {
        showSignoutAlert(mFragment, getString(R.string.logout_sure));
    }

    public void showSignoutAlert(Fragment mFragment, String strMessage) {
        final Dialog alertDialog = new Dialog(Objects.requireNonNull(getActivity()));
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.signout_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnNo = alertDialog.findViewById(R.id.btnNo);
        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        txtMessageTV.setText(strMessage);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }

            private void logout() {
                SharedPreferences preferences = ChecklistPreferences.getPreferences(Objects.requireNonNull(getActivity()));
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                getActivity().onBackPressed();
                Intent mIntent = new Intent(getActivity(), LoginActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mIntent);
            }
        });
        alertDialog.show();
    }



    @Override
    public void onResume() {
        super.onResume();
        getUserProfileDetails();
    }
    private void getUserProfileDetails() {
        if (!isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
        } else {
            executeGetUserDetailsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", ChecklistPreferences.readString(getActivity(),ChecklistPreferences.USER_ID,null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetUserDetailsApi() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(mParams()).enqueue(new Callback<ProfileModelStatus>() {
            @Override
            public void onResponse(Call<ProfileModelStatus> call, Response<ProfileModelStatus> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ProfileModelStatus mGetDetailsModel = response.body();
                assert mGetDetailsModel != null;
                if (mGetDetailsModel.getStatus().equalsIgnoreCase("1")) {
                    String imageurl = mGetDetailsModel.getData().getProfile_pic();
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    if( getActivity()!=null){
                    if (imageurl!=null ){
                        Glide.with(Objects.requireNonNull(getActivity()))
                                .load(imageurl)
                                .apply(options)
                                .into(imgProfileIV);}

                    if (imageurl!=null) {
                        Intent intent = new Intent("changeImage");
                        intent.putExtra("Status", "editImage");
                        intent.putExtra("editImage", imageurl);
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                    }
                    else {
                        String img=ChecklistPreferences.readString(getActivity(),ChecklistPreferences.PROFILE_PICTURE,null);
                        Glide.with(Objects.requireNonNull(getActivity()))
                                .load(img)
                                .apply(options)
                                .into(imgProfileIV);
                    }}
                    txtUserNameTV.setText(response.body().getData().getUsername());
                    txtEmailTV.setText(response.body().getData().getEmail());
                    txtChangePassTV.setText(response.body().getData().getPassword());
                    txtContactTV.setText(response.body().getData().getMoblie_number());
                } else {
                    assert response.body() != null;
                    if (response.body().getStatus().equalsIgnoreCase("0")) {
                        showAlertDialog(getActivity(), mGetDetailsModel.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileModelStatus> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder!=null){
        unbinder.unbind();}
    }
}


