package com.checklist.app.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.checklist.app.R;
import com.checklist.app.RetrofitApi.ApiClient;
import com.checklist.app.activities.ComplaintActivity;
import com.checklist.app.activities.ReattemptQuestionTopicActivity;
import com.checklist.app.interfaces.ApiInterface;
import com.checklist.app.model.ColorModel;
import com.checklist.app.utils.ChecklistPreferences;
import com.checklist.app.utils.Constants;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = HomeFragment.this;

    Unbinder unbinder;

    /*
     * Widgets
     * */
    RoundedHorizontalProgressBar progressBar1;
    RoundedHorizontalProgressBar progressBar2;
    RoundedHorizontalProgressBar progressBar3;
    CardView cardAttentionCV;
    CardView cardComplaintCV;
    CardView cardNotCompCV;
    TextView textAttentionTV;
    TextView textNotCompTV;
    TextView textComplaintTV;

    public HomeFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, v);
        progressBar1=v.findViewById(R.id.progressBar1);
        progressBar2=v.findViewById(R.id.progressBar2);
        progressBar3=v.findViewById(R.id.progressBar3);
        cardAttentionCV=v.findViewById(R.id.cardAttentionCV);
        cardComplaintCV=v.findViewById(R.id.cardComplaintCV);
        textAttentionTV=v.findViewById(R.id.textAttentionTV);
        cardNotCompCV=v.findViewById(R.id.cardNotCompCV);
        textNotCompTV=v.findViewById(R.id.textNotCompTV);
        textComplaintTV=v.findViewById(R.id.textComplaintTV);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        executeGetColorsApi();
    }

    //get colors and percentage of submitted questions
    private void executeGetColorsApi() {
        if (!isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
        } else {
            getColorsAndPercentage();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", ChecklistPreferences.readString(getActivity(), ChecklistPreferences.USER_ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getColorsAndPercentage() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getColor(mParam()).enqueue(new Callback<ColorModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ColorModel> call, Response<ColorModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ColorModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    if (mModel.getData().getGreen()!=null){
                    textComplaintTV.setText(mModel.getData().getGreen().toString().concat("%"));}
                    if (mModel.getData().getYellow()!=null){
                    textNotCompTV.setText(mModel.getData().getYellow().toString().concat("%"));}
                    if (mModel.getData().getRed()!=null){
                    textAttentionTV.setText(mModel.getData().getRed().toString().concat("%"));}
                    progressBar1.setProgress(mModel.getData().getGreen());
                    progressBar2.setProgress(mModel.getData().getYellow());
                    progressBar3.setProgress(mModel.getData().getRed());
                } else {
                    assert response.body() != null;
                    if (response.body().getStatus().equals("0")) {
                        showAlertDialog(getActivity(), mModel.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ColorModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @OnClick({R.id.cardComplaintCV, R.id.cardNotCompCV, R.id.cardAttentionCV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardComplaintCV:
                performComplaintClick();
                break;
            case R.id.cardNotCompCV:
                performNotComplaintClick();
                break;
            case R.id.cardAttentionCV:
                performAttentionClick();
                break;
        }
    }

    private void performAttentionClick() {
        if (progressBar3.getProgress() == 0) {
            cardAttentionCV.setClickable(false);
        } else {
            Intent intent = new Intent(getActivity(), ReattemptQuestionTopicActivity.class);
            intent.putExtra(Constants.SELECTED_COLOR, "red");
            startActivity(intent); }

    }

    private void performNotComplaintClick() {
        if (progressBar2.getProgress() == 0) {
            cardNotCompCV.setClickable(false);
        } else {
            Intent intent = new Intent(getActivity(), ReattemptQuestionTopicActivity.class);
            intent.putExtra(Constants.SELECTED_COLOR, "yellow");
            startActivity(intent);
        }
    }

    private void performComplaintClick() {
        if (progressBar1.getProgress() == 100) {
            Intent i = new Intent(getActivity(), ComplaintActivity.class);
            i.putExtra("completed", true);
            startActivity(i);
        } else {
                Intent i = new Intent(getActivity(), ComplaintActivity.class);
                i.putExtra("completed", false);
               startActivity(i);
    }}

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder!=null){
        unbinder.unbind();}
    }
}
