package com.checklist.app.interfaces;

import com.checklist.app.model.ChangePassStatusModel;
import com.checklist.app.model.ChangePasswordModel;
import com.checklist.app.model.ColorModel;
import com.checklist.app.model.ContactUsStatusModel;
import com.checklist.app.model.EditProfileStatusModel;
import com.checklist.app.model.ForgotPasswordModel;
import com.checklist.app.model.LoginModel;
import com.checklist.app.model.ProfileModelStatus;
import com.checklist.app.model.QuestionModel;
import com.checklist.app.model.SignUpModel;
import com.checklist.app.model.TopicsModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("Signup.php")
    Call<SignUpModel> signUpRequest(@Body Map<String, String> mParams);

    @POST("Login.php")
    Call<LoginModel> loginRequest(@Body Map<String, String> mParams);

    @POST("Forgotpassword.php")
    Call<ForgotPasswordModel> forgotPasswordRequest(@Body Map<String, String> mParams);

    @POST("GetTopicDetails.php")
    Call<TopicsModel> questionTopicRequest(@Body Map<String, String> mParams);

    @POST("ResumeQuiz.php")
    Call<QuestionModel> questionResumeRequest(@Body Map<String, String> mParams);

    @POST("QA_Topic.php")
    Call<QuestionModel> questionGetSubmit(@Body Map<String, String> mParams);

    @POST("ColorResult.php")
    Call<ColorModel> getColor(@Body Map<String, String> mParams);

    @POST("GetColorTopicDetails.php")
    Call<TopicsModel> getColorTopic(@Body Map<String, String> mParams);

    @POST("QA_ColorTopic.php")
    Call<QuestionModel> saveColorTopic(@Body Map<String, String> mParams);

    @POST("GetUserdetail.php")
    Call<ProfileModelStatus> getProfileDetailsRequest(@Body Map<String, String> mParams);

    @POST("EditProfile.php")
    Call<EditProfileStatusModel> saveProfileDetailsRequest(@Body Map<String, String> mParams);

    @POST("ChangePassword.php")
    Call<ChangePassStatusModel> addChangePassModel(@Body ChangePasswordModel changePasswordModel);

    @POST("ContactUs.php")
    Call<ContactUsStatusModel> contactUsRequest(@Body Map<String, String> mParams);

}