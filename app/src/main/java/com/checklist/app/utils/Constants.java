package com.checklist.app.utils;

public class Constants {
    /*
     * Constants @params
     * */
    public static String HOME_FRAGMENT = "home_fragment";
    public static String PROFILE_FRAGMENT = "profile_fragment";
    public static String FLAG_QUESTIONS = "question";
    public static String FLAG_TOPICS = "topic";
    public static String FLAG_HOME = "home";
    public static String TOPIC_ID = "topic_id";
    public static String QUESTION_ID = "question_id";
    public static String SELECTED_COLOR = "selected_color";

    /*
     * Terms & Conditions
     * */
    public static String TERMS_SERVICES = "https://www.dharmani.com/walearogundad/Webservice/terms&services.html";

    /*
     * @Api Urls
     * */
    private static String BASE_URL = "https://www.dharmani.com/walearogundad/Webservice/";


}
