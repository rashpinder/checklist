package com.checklist.app.utils;


import com.checklist.app.model.GetQuesStatusModel;

import java.util.ArrayList;

/**
 * Created by android-da on 5/19/18.
 */

public class CheckListSingleton {


    private static CheckListSingleton ourInstance;

    private CheckListSingleton() { }

    private ArrayList<GetQuesStatusModel.QList> questionsArrayList  = new ArrayList<>();



    public static CheckListSingleton getInstance() {
        if (ourInstance == null) {
            ourInstance = new CheckListSingleton();
        }
        return ourInstance;
    }

    public ArrayList<GetQuesStatusModel.QList> getQuestionsArrayList() {
        return questionsArrayList;
    }

    public void clearQuestionsList(){
        questionsArrayList.clear();
    }
}


