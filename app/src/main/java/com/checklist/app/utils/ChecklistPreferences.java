package com.checklist.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class ChecklistPreferences {

    /***************
     * Define SharedPrefrances Keys & MODE
     ****************/
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;
    /*
     * Keys
     * */
    public static final String USER_ID = "id";
    public static final String ISLOGIN = "is_login";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String QUES_SCR = "ques_screen";
    public static final String USERNAME = "username";
    public static final String ACTIVITY_FLAG = "activity_flag";
    public static final String PROFILE_PICTURE = "profile";
    public static final String MOBILE_NUMBER = "mobile_no";
    public static final String STATUS = "status";
    public static final String ID = "ques_id";
    public static final String TOPIC_ID = "topic_id";
    public static final String Ques_ID = "ques_id";
    public static final String Sel_Ans = "sel_ans";
    public static final String Sel_Col = "sel_col";
    public static final String ANSWER = "answer";
    public static final String COLOR = "color";
    public static final String SUB_TOPIC_ID = "sub_topic_id";


    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Value
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();

    }

    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}

