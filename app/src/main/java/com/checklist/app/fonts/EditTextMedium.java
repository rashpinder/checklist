package com.checklist.app.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by android-da on 5/23/18.
 */

@SuppressLint("AppCompatCustomView")
public class EditTextMedium extends EditText {

    /*
     * Getting Current Class Name
     * */
    String mTag = EditTextMedium.this.getClass().getSimpleName();



    /*
     * Constructor with
     * #Context
     * */
    public EditTextMedium(Context context) {
        super(context);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public EditTextMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public EditTextMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }


    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsMedium(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
