package com.checklist.app.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */

@SuppressLint("AppCompatCustomView")
public class EditTextBold extends EditText {

    /*
     * Getting Current Class Name
     * */
    private String mTag = EditTextBold.this.getClass().getSimpleName();


    /*
     * Constructor with
     * #Context
     * */
    public EditTextBold(Context context) {
        super(context);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public EditTextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public EditTextBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }



    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag, e.toString());
        }
    }
}
