package com.checklist.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.checklist.app.R;
import com.checklist.app.activities.BaseActivity;
import com.checklist.app.activities.QuestionsActivity;
import com.checklist.app.model.TopicsData;
import com.checklist.app.utils.Constants;

import java.util.ArrayList;

public class ReattemptTopicAdapter extends RecyclerView.Adapter<ReattemptTopicAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<TopicsData> mArrayList = new ArrayList<>();
    String selectedColor = "";

    /*constructor*/
    public ReattemptTopicAdapter(Activity mActivity, ArrayList<TopicsData> mArrayList, String selectedColor) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.selectedColor = selectedColor;
    }

    @Override
    public ReattemptTopicAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reattempt_topic_list, parent, false);
        return new MyViewHolder(itemView);
    }

    /*bind data*/
    @Override
    public void onBindViewHolder(final ReattemptTopicAdapter.MyViewHolder holder, final int position) {
        TopicsData mModel = mArrayList.get(position);

        //Set Topic Name
        holder.mTopicNameTV.setText(mModel.getName());

        if (mModel.getSubtopicName().size() > 0) {
            holder.imgDownIV.setVisibility(View.VISIBLE);
            //Set Subcategory DropDown
            ReattemptSubTopicAdapter mSubTopicAdapter = new ReattemptSubTopicAdapter(mActivity, mModel.getSubtopicName(),selectedColor);
            holder.mSubRecyclerViewRV.setLayoutManager(new LinearLayoutManager(mActivity));
            holder.mSubRecyclerViewRV.setAdapter(mSubTopicAdapter);
            if (mModel.getPendingQuestion() == 0) {
                holder.imgCompleteIV.setVisibility(View.VISIBLE);
            } else {
                holder.imgCompleteIV.setVisibility(View.VISIBLE);
                holder.imgCompleteIV.setImageResource(R.drawable.ic_close_black_24dp);
            }
        } else {
            holder.imgDownIV.setVisibility(View.GONE);
            if (mModel.getPendingQuestion() == 0) {
                holder.imgCompleteIV.setVisibility(View.VISIBLE);
            } else {
                holder.imgCompleteIV.setVisibility(View.GONE);
                holder.imgCompleteIV.setVisibility(View.VISIBLE);
                holder.imgCompleteIV.setImageResource(R.drawable.ic_close_black_24dp);
            }
        }

        /*
         * Set DropDownClick
         * */
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mModel.isActive()) {
                    if (mModel.getSubtopicName().size() > 0) {
                        if (holder.mSubRecyclerViewRV.getVisibility() == View.VISIBLE) {
                            holder.imgDownIV.setImageResource(R.drawable.ic_down_arrow_24);
                            holder.mSubRecyclerViewRV.setVisibility(View.GONE);
                        } else {
                            holder.imgDownIV.setImageResource(R.drawable.ic_up_arrow_24);
                            holder.mSubRecyclerViewRV.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Intent i = new Intent(mActivity, QuestionsActivity.class);
                        i.putExtra(Constants.TOPIC_ID, "" + mModel.getId());
                        i.putExtra(Constants.QUESTION_ID, "");
                        i.putExtra(Constants.SELECTED_COLOR, selectedColor);
                        mActivity.startActivity(i);
                    }
                } else {
                    ((BaseActivity) mActivity).showToast(mActivity, mActivity.getString(R.string.please_comple));
                }
            }
        });


        if (position < mArrayList.size() - 1) {
            holder.mView.setVisibility(View.VISIBLE);
        } else {
            holder.mView.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mTopicNameTV;
        RecyclerView mSubRecyclerViewRV;
        ImageView imgDownIV, imgCompleteIV;
        View mView;

        public MyViewHolder(View view) {
            super(view);
            mTopicNameTV = view.findViewById(R.id.mTopicNameTV);
            mSubRecyclerViewRV = view.findViewById(R.id.mSubRecyclerViewRV);
            imgDownIV = view.findViewById(R.id.imgDownIV);
            mView = view.findViewById(R.id.mView);
            imgCompleteIV = view.findViewById(R.id.imgCompleteIV);
        }
    }
}