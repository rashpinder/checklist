package com.checklist.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import androidx.recyclerview.widget.RecyclerView;

import com.checklist.app.R;
import com.checklist.app.interfaces.ClickedValueInterface;

import java.util.ArrayList;
import java.util.List;

public class TypeOneAdapter extends RecyclerView.Adapter<TypeOneAdapter.MyViewHolder> {
    Activity mActivity;
    private List<String> mList = new ArrayList<>();
    private ClickedValueInterface clickedValueInterface;
    private int lastSelectedPosition = -1;

    public TypeOneAdapter(Activity mActivity, ArrayList<String> mList, ClickedValueInterface clickedValueInterface) {
        this.mList = mList;
        this.mActivity = mActivity;
        this.clickedValueInterface = clickedValueInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view_type1, parent, false);

        return new TypeOneAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.txtOptionTV.setText(mList.get(listPosition));

        holder.txtOptionTV.setChecked(lastSelectedPosition == listPosition);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

       public RadioButton txtOptionTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtOptionTV = itemView.findViewById(R.id.txtOptionTV);

            txtOptionTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickedValueInterface.getItem(mList.get(getAdapterPosition()));
                    lastSelectedPosition =getAdapterPosition();
                    notifyDataSetChanged();
                }
            });

        }
    }


}
