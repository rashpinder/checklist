package com.checklist.app.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.checklist.app.R;
import com.checklist.app.interfaces.ClickedValueInterface;
import com.checklist.app.interfaces.QuestionsInterface;
import com.checklist.app.model.GetQAStatusModel;
import com.checklist.app.model.QuestionDataModel;
import com.checklist.app.model.StateListItem;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class QuestionsPagerAdapter extends PagerAdapter {

    Activity mActivity;
    ArrayList<QuestionDataModel> mArrayList = new ArrayList<>();
    ArrayList<String> mList = new ArrayList<>();
    LayoutInflater mLayoutInflater;
    QuestionsInterface questionsInterface;
    String ans, color, question_id;
    String topic_id;

    public QuestionsPagerAdapter(Activity mActivity, ArrayList<QuestionDataModel> mArrayList, QuestionsInterface questionsInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.questionsInterface = questionsInterface;
        mLayoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //get count
    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    //instantiate item
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // Inflate a new layout from our resources
        View view = mLayoutInflater.inflate(R.layout.item_question_list, container, false);

        TextView txtQuesTV = view.findViewById(R.id.txtQuesTV);
        LinearLayout type1LL = view.findViewById(R.id.type1LL);
        LinearLayout type2LL = view.findViewById(R.id.type2LL);
        TextView txtNoTV = view.findViewById(R.id.txtNoTV);
        TextView txtYesTV = view.findViewById(R.id.txtYesTV);
        Spinner mSpinner = view.findViewById(R.id.mSpinner);
        LinearLayout type3LL = view.findViewById(R.id.type3LL);
        RecyclerView recyclerViewType1RV = view.findViewById(R.id.recyclerViewType1RV);

        QuestionDataModel mModel = mArrayList.get(position);

        /*
        if question is of type 1
         */
        if (mArrayList != null) {
            if (mArrayList.get(position).getQuestionsType().equals("que_type_1")) {
                type1LL.setVisibility(View.VISIBLE);
                type2LL.setVisibility(View.GONE);
                type3LL.setVisibility(View.GONE);

                // Retrieve a TextView from the inflated View, and update it's text
                txtQuesTV.setText(mModel.getQuestionName());
                mList.addAll(mArrayList.get(position).getInCategory());


                TypeOneAdapter mTypeOneAdapter = new TypeOneAdapter(mActivity, mList, mClickedValueInterface);
                recyclerViewType1RV.setLayoutManager(new LinearLayoutManager(mActivity));
                recyclerViewType1RV.setAdapter(mTypeOneAdapter);

                color = "";
                if (mModel.getSubtopicId().equalsIgnoreCase("0")) {
                    topic_id = mModel.getTopicId();
                } else {
                    topic_id = mModel.getSubtopicId();
                }
                question_id = mModel.getId();
            }
        /*
        if ques is of type 2
         */
            else if (mArrayList.get(position).getQuestionsType().equalsIgnoreCase("que_type_2")) {
                type1LL.setVisibility(View.GONE);
                type2LL.setVisibility(View.VISIBLE);
                type3LL.setVisibility(View.GONE);

                txtQuesTV.setText(mModel.getQuestionName());
                txtYesTV.setText(mModel.get1stAnswer());
                txtNoTV.setText(mModel.get2ndAnswer());
                if (mModel.getSelected_answer() != null) {
                    if (mModel.getSelected_answer().equals(mModel.get1stAnswer())) {
                        txtYesTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn));
                        txtNoTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn_unselect));
                        txtNoTV.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                        ans = mModel.get1stAnswer();
                        color = mModel.get1stColor();
                        if (mModel.getSubtopicId().equalsIgnoreCase("0")) {
                            topic_id = mModel.getTopicId();
                        } else {
                            topic_id = mModel.getSubtopicId();
                        }
                        question_id = mModel.getId();
                        questionsInterface.onClick(topic_id, question_id, ans, color);
                    } else {
                        txtNoTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn));
                        txtYesTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn_unselect));
                        txtYesTV.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                        ans = mModel.get2ndAnswer();
                        color = mModel.get2ndColor();
                        if (mModel.getSubtopicId().equalsIgnoreCase("0")) {
                            topic_id = mModel.getTopicId();
                        } else {
                            topic_id = mModel.getSubtopicId();
                        }
                        question_id = mModel.getId();
                        questionsInterface.onClick(topic_id, question_id, ans, color);
                    }
                } else {
                    txtNoTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn_unselect));
                    txtYesTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn_unselect));
                    txtYesTV.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                    txtNoTV.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                }
                txtYesTV.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(View view) {
                        txtYesTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn));
                        txtNoTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn_unselect));
                        txtNoTV.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                        txtYesTV.setTextColor(mActivity.getResources().getColor(R.color.colorWhite));
                        ans = mModel.get1stAnswer();
                        color = mModel.get1stColor();
                        if (mModel.getSubtopicId().equalsIgnoreCase("0")) {
                            topic_id = mModel.getTopicId();
                        } else {
                            topic_id = mModel.getSubtopicId();
                        }
                        question_id = mModel.getId();
                        questionsInterface.onClick(topic_id, question_id, ans, color);
                    }
                });
                txtNoTV.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(View view) {
                        txtYesTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn_unselect));
                        txtNoTV.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_btn));
                        txtYesTV.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                        txtNoTV.setTextColor(mActivity.getResources().getColor(R.color.colorWhite));
                        ans = mModel.get2ndAnswer();
                        color = mModel.get2ndColor();
                        if (mModel.getSubtopicId().equalsIgnoreCase("0")) {
                            topic_id = mModel.getTopicId();
                        } else {
                            topic_id = mModel.getSubtopicId();
                        }
                        question_id = mModel.getId();
                        questionsInterface.onClick(topic_id, question_id, ans, color);
                    }
                });
            }
        /*
        if ques is of type 3
         */
            else {
                type1LL.setVisibility(View.GONE);
                type2LL.setVisibility(View.GONE);
                type3LL.setVisibility(View.VISIBLE);

                txtQuesTV.setText(mModel.getQuestionName());
                if (mModel.getStateList() != null) {
                    ArrayAdapter<StateListItem> adapter = new ArrayAdapter<StateListItem>(mActivity, android.R.layout.simple_spinner_item, mModel.getStateList()) {
                        public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                            View v = convertView;
                            if (v == null) {
                                Context mContext = this.getContext();
                                LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                assert vi != null;
                                v = vi.inflate(R.layout.spinner_item, null);
                            }
                            TextView itemTextView = v.findViewById(R.id.itemTV);
                            itemTextView.setText(mModel.getStateList().get(position).getName());
                            return v;
                        }

                    };
                    mSpinner.setAdapter(adapter);
                    /*Status Spinner Click Listener*/
                    mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            ((TextView) view).setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                            ((TextView) view).setText(mModel.getStateList().get(position).getName());
                            mSpinner.setSelection(position);
                            ans = mModel.getStateList().get(position).getName();
                            if (mModel.getSubtopicId().equalsIgnoreCase("0")) {
                                topic_id = mModel.getTopicId();
                            } else {
                                topic_id = mModel.getSubtopicId();
                            }
                            question_id = mModel.getId();
                            questionsInterface.onClick(topic_id, question_id, ans, color);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            ans = mModel.getStateList().get(0).getName();
                        }
                    });

                }
            }
        }
        view.setTag(mModel);
        // Add the newly created View to the ViewPager
        container.addView(view);
        Log.i(TAG, "instantiateItem() [position: " + position + "]" + " childCount:" + container.getChildCount());
        // Return the View
        return view;
    }


    ClickedValueInterface mClickedValueInterface = new ClickedValueInterface() {
        @Override
        public void getItem(String item) {
            ans = item;
            Log.e(TAG, "Type1 Answer: " + ans);
            questionsInterface.onClick(topic_id, question_id, ans, color);
        }
    };


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        Log.i(TAG, "destroyItem() [position: " + position + "]" + " childCount:" + container.getChildCount());
    }
}