package com.checklist.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.checklist.app.R;
import com.checklist.app.activities.BaseActivity;
import com.checklist.app.activities.QuestionsActivity;
import com.checklist.app.model.SubTopicsData;
import com.checklist.app.utils.Constants;

import java.util.ArrayList;

public class SubTopicAdapter extends RecyclerView.Adapter<SubTopicAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<SubTopicsData> mArrayList = new ArrayList<>();

    /*constructor*/
    SubTopicAdapter(Activity mActivity, ArrayList<SubTopicsData> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public SubTopicAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sub_topic_list, parent, false);

        return new MyViewHolder(itemView);
    }

    //bind data
    @Override
    public void onBindViewHolder(SubTopicAdapter.MyViewHolder holder, final int position) {
        SubTopicsData mModel = mArrayList.get(position);
        //Set Subtopic Name
        holder.mSubTopicNameTV.setText(mModel.getSubtopicName());


        if (mModel.getPendingQuestion() == 0) {
            holder.imgCompleteIV.setVisibility(View.VISIBLE);
        } else {
            holder.imgCompleteIV.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mModel.isActive() == true){
                    Intent i = new Intent(mActivity, QuestionsActivity.class);
                    i.putExtra(Constants.TOPIC_ID, "" + mModel.getId());
                    i.putExtra(Constants.QUESTION_ID, "");
                    mActivity.startActivity(i);
                }else{
                    ((BaseActivity)mActivity).showToast(mActivity, mActivity.getString(R.string.please_comple));
                }

            }
        });


    }

    //get item count
    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mSubTopicNameTV;
        ImageView imgCompleteIV;

        MyViewHolder(View view) {
            super(view);
            mSubTopicNameTV = view.findViewById(R.id.mSubTopicNameTV);
            imgCompleteIV = view.findViewById(R.id.imgCompleteIV);
        }
    }

}