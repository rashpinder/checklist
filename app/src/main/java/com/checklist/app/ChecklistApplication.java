package com.checklist.app;

import android.app.Application;

public class ChecklistApplication extends Application
{
        /**
         * Getting the Current Class Name
         */
        public static final String TAG = ChecklistApplication.class.getSimpleName();
    /**
     * Initialize the Applications Instance
     */
    private static ChecklistApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        mInstance = this;
    }

}
