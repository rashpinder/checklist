package com.checklist.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
public class GetQuesStatusModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<QList> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<QList> getData() {
        return data;
    }

    public void setData(List<QList> data) {
        this.data = data;
    }

public class QList {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("topic_id")
        @Expose
        private String topicId;
        @SerializedName("subtopic_id")
        @Expose
        private int subtopicId;
        @SerializedName("parent_question")
        @Expose
        private String parentQuestion;
        @SerializedName("question_name")
        @Expose
        private String questionName;
        @SerializedName("questions_type")
        @Expose
        private String questionsType;
        @SerializedName("in_category")
        @Expose
        private String inCategory;
        @SerializedName("in_category2")
        @Expose
        private String inCategory2;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("is_parent")
        @Expose
        private String isParent;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("1st_answer")
        @Expose
        private String _1stAnswer;
        @SerializedName("2nd_answer")
        @Expose
        private String _2ndAnswer;
        @SerializedName("1st_color")
        @Expose
        private String _1stColor;
        @SerializedName("2nd_color")
        @Expose
        private String _2ndColor;
        @SerializedName("create_date")
        @Expose
        private String createDate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTopicId() {
            return topicId;
        }

        public void setTopicId(String topicId) {
            this.topicId = topicId;
        }

        public int getSubtopicId() {
            return subtopicId;
        }

        public void setSubtopicId(int subtopicId) {
            this.subtopicId = subtopicId;
        }

        public String getParentQuestion() {
            return parentQuestion;
        }

        public void setParentQuestion(String parentQuestion) {
            this.parentQuestion = parentQuestion;
        }

        public String getQuestionName() {
            return questionName;
        }

        public void setQuestionName(String questionName) {
            this.questionName = questionName;
        }

        public String getQuestionsType() {
            return questionsType;
        }

        public void setQuestionsType(String questionsType) {
            this.questionsType = questionsType;
        }

        public String getInCategory() {
            return inCategory;
        }

        public void setInCategory(String inCategory) {
            this.inCategory = inCategory;
        }

        public String getInCategory2() {
            return inCategory2;
        }

        public void setInCategory2(String inCategory2) {
            this.inCategory2 = inCategory2;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public String getIsParent() {
            return isParent;
        }

        public void setIsParent(String isParent) {
            this.isParent = isParent;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String get1stAnswer() {
            return _1stAnswer;
        }

        public void set1stAnswer(String _1stAnswer) {
            this._1stAnswer = _1stAnswer;
        }

        public String get2ndAnswer() {
            return _2ndAnswer;
        }

        public void set2ndAnswer(String _2ndAnswer) {
            this._2ndAnswer = _2ndAnswer;
        }

        public String get1stColor() {
            return _1stColor;
        }

        public void set1stColor(String _1stColor) {
            this._1stColor = _1stColor;
        }

        public String get2ndColor() {
            return _2ndColor;
        }

        public void set2ndColor(String _2ndColor) {
            this._2ndColor = _2ndColor;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

    }}

