package com.checklist.app.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TopicsData {

	@SerializedName("subtopic_name")
	private ArrayList<SubTopicsData> subtopicName;

	@SerializedName("name")
	private String name;

	@SerializedName("active")
	private boolean active;

	@SerializedName("id")
	private int id;

	@SerializedName("pending_question")
	private int pendingQuestion;

	public ArrayList<SubTopicsData> getSubtopicName(){
		return subtopicName;
	}

	public String getName(){
		return name;
	}

	public boolean isActive(){
		return active;
	}

	public int getId(){
		return id;
	}

	public int getPendingQuestion(){
		return pendingQuestion;
	}


	public void setSubtopicName(ArrayList<SubTopicsData> subtopicName) {
		this.subtopicName = subtopicName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPendingQuestion(int pendingQuestion) {
		this.pendingQuestion = pendingQuestion;
	}

	@Override
	public String toString() {
		return "TopicsData{" +
				"name='" + name + '\'' +
				", active='" + active + '\'' +
				", pending_question='" + pendingQuestion + '\'' +
				", subtopicName='" + subtopicName + '\'' +
				'}';
	}
}