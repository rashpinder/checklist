package com.checklist.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionDataModel {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("topic_id")
	@Expose
	private String topicId;
	@SerializedName("subtopic_id")
	@Expose
	private String subtopicId;
	@SerializedName("parent_question")
	@Expose
	private String parentQuestion;
	@SerializedName("question_name")
	@Expose
	private String questionName;
	@SerializedName("questions_type")
	@Expose
	private String questionsType;
	@SerializedName("in_category")
	@Expose
	private List<String> inCategory = null;
	@SerializedName("in_category2")
	@Expose
	private String inCategory2;
	@SerializedName("disable")
	@Expose
	private String disable;
	@SerializedName("is_parent")
	@Expose
	private String isParent;
	@SerializedName("creation_date")
	@Expose
	private String creationDate;
	@SerializedName("1st_answer")
	@Expose
	private String _1stAnswer;
	@SerializedName("2nd_answer")
	@Expose
	private String _2ndAnswer;
	@SerializedName("1st_color")
	@Expose
	private String _1stColor;
	@SerializedName("2nd_color")
	@Expose
	private String _2ndColor;
	@SerializedName("create_date")
	@Expose
	private String createDate;

	@SerializedName("selected_answer")
	@Expose
	private String selected_answer;

	@SerializedName("state_list")
	@Expose
	private List<StateListItem> stateList = null;

	public String getSelected_answer() {
		return selected_answer;
	}

	public void setSelected_answer(String selected_answer) {
		this.selected_answer = selected_answer;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	public String getSubtopicId() {
		return subtopicId;
	}

	public void setSubtopicId(String subtopicId) {
		this.subtopicId = subtopicId;
	}

	public String getParentQuestion() {
		return parentQuestion;
	}

	public void setParentQuestion(String parentQuestion) {
		this.parentQuestion = parentQuestion;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getQuestionsType() {
		return questionsType;
	}

	public void setQuestionsType(String questionsType) {
		this.questionsType = questionsType;
	}

	public List<String> getInCategory() {
		return inCategory;
	}

	public void setInCategory(List<String> inCategory) {
		this.inCategory = inCategory;
	}

	public String getInCategory2() {
		return inCategory2;
	}

	public void setInCategory2(String inCategory2) {
		this.inCategory2 = inCategory2;
	}

	public String getDisable() {
		return disable;
	}

	public void setDisable(String disable) {
		this.disable = disable;
	}

	public String getIsParent() {
		return isParent;
	}

	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String get1stAnswer() {
		return _1stAnswer;
	}

	public void set1stAnswer(String _1stAnswer) {
		this._1stAnswer = _1stAnswer;
	}

	public String get2ndAnswer() {
		return _2ndAnswer;
	}

	public void set2ndAnswer(String _2ndAnswer) {
		this._2ndAnswer = _2ndAnswer;
	}

	public String get1stColor() {
		return _1stColor;
	}

	public void set1stColor(String _1stColor) {
		this._1stColor = _1stColor;
	}

	public String get2ndColor() {
		return _2ndColor;
	}

	public void set2ndColor(String _2ndColor) {
		this._2ndColor = _2ndColor;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public List<StateListItem> getStateList() {
		return stateList;
	}

	public void setStateList(List<StateListItem> stateList) {
		this.stateList = stateList;
	}

	@Override
	public String toString() {
		return "QuestionDataModel{" +
				"id='" + id + '\'' +
				", topicId='" + topicId + '\'' +
				", subtopicId='" + subtopicId + '\'' +
				", parentQuestion='" + parentQuestion + '\'' +
				", questionName='" + questionName + '\'' +
				", questionsType='" + questionsType + '\'' +
				", inCategory=" + inCategory +
				", inCategory2='" + inCategory2 + '\'' +
				", disable='" + disable + '\'' +
				", isParent='" + isParent + '\'' +
				", creationDate='" + creationDate + '\'' +
				", _1stAnswer='" + _1stAnswer + '\'' +
				", _2ndAnswer='" + _2ndAnswer + '\'' +
				", _1stColor='" + _1stColor + '\'' +
				", _2ndColor='" + _2ndColor + '\'' +
				", createDate='" + createDate + '\'' +
				", stateList=" + stateList +
				'}';
	}
}
//package com.checklist.app.model;
//
//import java.util.List;
//import com.google.gson.annotations.SerializedName;
//
//public class QuestionDataModel {
//
//	@SerializedName("1st_color")
//	private String jsonMember1stColor;
//
//	@SerializedName("1st_answer")
//	private String jsonMember1stAnswer;
//
//	@SerializedName("state_list")
//	private List<StateListItem> stateList;
//
//	@SerializedName("creation_date")
//	private String creationDate;
//
//	@SerializedName("is_parent")
//	private String isParent;
//
//	@SerializedName("in_category2")
//	private String inCategory2;
//
//	@SerializedName("parent_question")
//	private String parentQuestion;
//
//	@SerializedName("disable")
//	private String disable;
//
//	@SerializedName("question_name")
//	private String questionName;
//
//	@SerializedName("2nd_answer")
//	private String jsonMember2ndAnswer;
//
//	@SerializedName("id")
//	private String id;
//
//	@SerializedName("topic_id")
//	private String topicId;
//
//	@SerializedName("subtopic_id")
//	private String subtopicId;
//
//	@SerializedName("in_category")
//	private String inCategory;
//
//	@SerializedName("create_date")
//	private String createDate;
//
//	@SerializedName("questions_type")
//	private String questionsType;
//
//	@SerializedName("2nd_color")
//	private String jsonMember2ndColor;
//
//	public void setJsonMember1stColor(String jsonMember1stColor){
//		this.jsonMember1stColor = jsonMember1stColor;
//	}
//
//	public String getJsonMember1stColor(){
//		return jsonMember1stColor;
//	}
//
//	public void setJsonMember1stAnswer(String jsonMember1stAnswer){
//		this.jsonMember1stAnswer = jsonMember1stAnswer;
//	}
//
//	public String getJsonMember1stAnswer(){
//		return jsonMember1stAnswer;
//	}
//
//	public void setStateList(List<StateListItem> stateList){
//		this.stateList = stateList;
//	}
//
//	public List<StateListItem> getStateList(){
//		return stateList;
//	}
//
//	public void setCreationDate(String creationDate){
//		this.creationDate = creationDate;
//	}
//
//	public String getCreationDate(){
//		return creationDate;
//	}
//
//	public void setIsParent(String isParent){
//		this.isParent = isParent;
//	}
//
//	public String getIsParent(){
//		return isParent;
//	}
//
//	public void setInCategory2(String inCategory2){
//		this.inCategory2 = inCategory2;
//	}
//
//	public String getInCategory2(){
//		return inCategory2;
//	}
//
//	public void setParentQuestion(String parentQuestion){
//		this.parentQuestion = parentQuestion;
//	}
//
//	public String getParentQuestion(){
//		return parentQuestion;
//	}
//
//	public void setDisable(String disable){
//		this.disable = disable;
//	}
//
//	public String getDisable(){
//		return disable;
//	}
//
//	public void setQuestionName(String questionName){
//		this.questionName = questionName;
//	}
//
//	public String getQuestionName(){
//		return questionName;
//	}
//
//	public void setJsonMember2ndAnswer(String jsonMember2ndAnswer){
//		this.jsonMember2ndAnswer = jsonMember2ndAnswer;
//	}
//
//	public String getJsonMember2ndAnswer(){
//		return jsonMember2ndAnswer;
//	}
//
//	public void setId(String id){
//		this.id = id;
//	}
//
//	public String getId(){
//		return id;
//	}
//
//	public void setTopicId(String topicId){
//		this.topicId = topicId;
//	}
//
//	public String getTopicId(){
//		return topicId;
//	}
//
//	public void setSubtopicId(String subtopicId){
//		this.subtopicId = subtopicId;
//	}
//
//	public String getSubtopicId(){
//		return subtopicId;
//	}
//
//	public void setInCategory(String inCategory){
//		this.inCategory = inCategory;
//	}
//
//	public String getInCategory(){
//		return inCategory;
//	}
//
//	public void setCreateDate(String createDate){
//		this.createDate = createDate;
//	}
//
//	public String getCreateDate(){
//		return createDate;
//	}
//
//	public void setQuestionsType(String questionsType){
//		this.questionsType = questionsType;
//	}
//
//	public String getQuestionsType(){
//		return questionsType;
//	}
//
//	public void setJsonMember2ndColor(String jsonMember2ndColor){
//		this.jsonMember2ndColor = jsonMember2ndColor;
//	}
//
//	public String getJsonMember2ndColor(){
//		return jsonMember2ndColor;
//	}
//
//	@Override
// 	public String toString(){
//		return
//			"Data{" +
//			"1st_color = '" + jsonMember1stColor + '\'' +
//			",1st_answer = '" + jsonMember1stAnswer + '\'' +
//			",state_list = '" + stateList + '\'' +
//			",creation_date = '" + creationDate + '\'' +
//			",is_parent = '" + isParent + '\'' +
//			",in_category2 = '" + inCategory2 + '\'' +
//			",parent_question = '" + parentQuestion + '\'' +
//			",disable = '" + disable + '\'' +
//			",question_name = '" + questionName + '\'' +
//			",2nd_answer = '" + jsonMember2ndAnswer + '\'' +
//			",id = '" + id + '\'' +
//			",topic_id = '" + topicId + '\'' +
//			",subtopic_id = '" + subtopicId + '\'' +
//			",in_category = '" + inCategory + '\'' +
//			",create_date = '" + createDate + '\'' +
//			",questions_type = '" + questionsType + '\'' +
//			",2nd_color = '" + jsonMember2ndColor + '\'' +
//			"}";
//		}
//}