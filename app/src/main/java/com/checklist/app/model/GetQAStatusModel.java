package com.checklist.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetQAStatusModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("pending_count_curr_topic")
    @Expose
    private Integer pendingCountCurrTopic;
    @SerializedName("overall_pending")
    @Expose
    private Integer overallPending;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getPendingCountCurrTopic() {
        return pendingCountCurrTopic;
    }

    public void setPendingCountCurrTopic(Integer pendingCountCurrTopic) {
        this.pendingCountCurrTopic = pendingCountCurrTopic;
    }

    public Integer getOverallPending() {
        return overallPending;
    }

    public void setOverallPending(Integer overallPending) {
        this.overallPending = overallPending;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("topic_id")
        @Expose
        private int topicId;
        @SerializedName("subtopic_id")
        @Expose
        private String subtopicId;
        @SerializedName("parent_question")
        @Expose
        private String parentQuestion;
        @SerializedName("question_id")
        @Expose
        private int question_id;

        @SerializedName("question_name")
        @Expose
        private String questionName;
        @SerializedName("questions_type")
        @Expose
        private String questionsType;
        @SerializedName("state_list")
        @Expose
        private List<StateList> stateList = null;
        @SerializedName("in_category2")
        @Expose
        private String inCategory2;

        @SerializedName("in_category")
        @Expose
        private String inCategory;

        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("is_parent")
        @Expose
        private String isParent;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("1st_answer")
        @Expose
        private String _1stAnswer;
        @SerializedName("2nd_answer")
        @Expose
        private String _2ndAnswer;
        @SerializedName("1st_color")
        @Expose
        private String _1stColor;
        @SerializedName("2nd_color")
        @Expose
        private String _2ndColor;
        @SerializedName("create_date")
        @Expose
        private String createDate;

        public int getQuestion_id() {
            return question_id;
        }

        public void setQuestion_id(int question_id) {
            this.question_id = question_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getTopicId() {
            return topicId;
        }

        public void setTopicId(int topicId) {
            this.topicId = topicId;
        }

        public String getSubtopicId() {
            return subtopicId;
        }

        public void setSubtopicId(String subtopicId) {
            this.subtopicId = subtopicId;
        }

        public String getParentQuestion() {
            return parentQuestion;
        }

        public void setParentQuestion(String parentQuestion) {
            this.parentQuestion = parentQuestion;
        }

        public String getQuestionName() {
            return questionName;
        }

        public void setQuestionName(String questionName) {
            this.questionName = questionName;
        }

        public String getQuestionsType() {
            return questionsType;
        }

        public void setQuestionsType(String questionsType) {
            this.questionsType = questionsType;
        }

        public List<StateList> getStateList() {
            return stateList;
        }

        public void setStateList(List<StateList> stateList) {
            this.stateList = stateList;
        }

        public String getInCategory() {
            return inCategory;
        }

        public void setInCategory(String inCategory) {
            this.inCategory = inCategory;
        }

        public String getInCategory2() {
            return inCategory2;
        }

        public void setInCategory2(String inCategory2) {
            this.inCategory2 = inCategory2;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public String getIsParent() {
            return isParent;
        }

        public void setIsParent(String isParent) {
            this.isParent = isParent;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String get1stAnswer() {
            return _1stAnswer;
        }

        public void set1stAnswer(String _1stAnswer) {
            this._1stAnswer = _1stAnswer;
        }

        public String get2ndAnswer() {
            return _2ndAnswer;
        }

        public void set2ndAnswer(String _2ndAnswer) {
            this._2ndAnswer = _2ndAnswer;
        }

        public String get1stColor() {
            return _1stColor;
        }

        public void set1stColor(String _1stColor) {
            this._1stColor = _1stColor;
        }

        public String get2ndColor() {
            return _2ndColor;
        }

        public void set2ndColor(String _2ndColor) {
            this._2ndColor = _2ndColor;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }


        public class StateList {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("abbr")
            @Expose
            private String abbr;
            @SerializedName("name")
            @Expose
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getAbbr() {
                return abbr;
            }

            public void setAbbr(String abbr) {
                this.abbr = abbr;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }


            @Override
            public String toString() {
                return "StateList{" +
                        "id='" + id + '\'' +
                        ", abbr='" + abbr + '\'' +
                        ", name='" + name + '\'' +
                        '}';
            }
        }


        @Override
        public String toString() {
            return "Data{" +
                    "id='" + id + '\'' +
                    ", topicId=" + topicId +
                    ", subtopicId='" + subtopicId + '\'' +
                    ", parentQuestion='" + parentQuestion + '\'' +
                    ", question_id=" + question_id +
                    ", questionName='" + questionName + '\'' +
                    ", questionsType='" + questionsType + '\'' +
                    ", stateList=" + stateList +
                    ", inCategory2='" + inCategory2 + '\'' +
                    ", inCategory='" + inCategory + '\'' +
                    ", disable='" + disable + '\'' +
                    ", isParent='" + isParent + '\'' +
                    ", creationDate='" + creationDate + '\'' +
                    ", _1stAnswer='" + _1stAnswer + '\'' +
                    ", _2ndAnswer='" + _2ndAnswer + '\'' +
                    ", _1stColor='" + _1stColor + '\'' +
                    ", _2ndColor='" + _2ndColor + '\'' +
                    ", createDate='" + createDate + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "GetQAStatusModel{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", pendingCountCurrTopic=" + pendingCountCurrTopic +
                ", overallPending=" + overallPending +
                '}';
    }
}