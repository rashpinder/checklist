package com.checklist.app.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactUsStatusModel {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;

    public ContactUsStatusModel() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

@NonNull
    @Override
    public String toString() {
        return "ContactUsStatusModel{" +
                "message='" + message + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
