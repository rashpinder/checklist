package com.checklist.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TopicsModel {

    @SerializedName("data")
    private ArrayList<TopicsData> data;

    @SerializedName("overall_pending")
    private int overallPending;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private int status;

    public ArrayList<TopicsData> getData() {
        return data;
    }

    public int getOverallPending() {
        return overallPending;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }


	@Override
	public String toString() {
		return "TopicsModel{" +
				"message='" + message + '\'' +
				", status='" + status + '\'' +
				", data='" + data + '\'' +
				'}';
	}
}