package com.checklist.app.model;

import com.checklist.app.model.QuestionDataModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionModel {

	@SerializedName("status")
	@Expose
	private Integer status;
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("data")
	@Expose
	private QuestionDataModel data;
	@SerializedName("pending_count_curr_topic")
	@Expose
	private Integer pendingCountCurrTopic;
	@SerializedName("overall_pending")
	@Expose
	private Integer overallPending;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public QuestionDataModel getData() {
		return data;
	}

	public void setData(QuestionDataModel data) {
		this.data = data;
	}

	public Integer getPendingCountCurrTopic() {
		return pendingCountCurrTopic;
	}

	public void setPendingCountCurrTopic(Integer pendingCountCurrTopic) {
		this.pendingCountCurrTopic = pendingCountCurrTopic;
	}

	public Integer getOverallPending() {
		return overallPending;
	}

	public void setOverallPending(Integer overallPending) {
		this.overallPending = overallPending;
	}

	@Override
	public String toString() {
		return "QuestionModel{" +
				"status=" + status +
				", message='" + message + '\'' +
				", data=" + data +
				", pendingCountCurrTopic=" + pendingCountCurrTopic +
				", overallPending=" + overallPending +
				'}';
	}
}
//package com.checklist.app.model;
//
//import com.google.gson.annotations.SerializedName;
//
//public class QuestionModel {
//
//	@SerializedName("pending_count_curr_topic")
//	private int pendingCountCurrTopic;
//
//	@SerializedName("data")
//	private QuestionDataModel data;
//
//	@SerializedName("overall_pending")
//	private int overallPending;
//
//	@SerializedName("topic_id")
//	private String topicId;
//
//	@SerializedName("message")
//	private String message;
//
//	@SerializedName("status")
//	private int status;
//
//	public void setPendingCountCurrTopic(int pendingCountCurrTopic){
//		this.pendingCountCurrTopic = pendingCountCurrTopic;
//	}
//
//	public int getPendingCountCurrTopic(){
//		return pendingCountCurrTopic;
//	}
//
//	public void setData(QuestionDataModel data){
//		this.data = data;
//	}
//
//	public QuestionDataModel getData(){
//		return data;
//	}
//
//	public void setOverallPending(int overallPending){
//		this.overallPending = overallPending;
//	}
//
//	public int getOverallPending(){
//		return overallPending;
//	}
//
//	public void setTopicId(String topicId){
//		this.topicId = topicId;
//	}
//
//	public String getTopicId(){
//		return topicId;
//	}
//
//	public void setMessage(String message){
//		this.message = message;
//	}
//
//	public String getMessage(){
//		return message;
//	}
//
//	public void setStatus(int status){
//		this.status = status;
//	}
//
//	public int getStatus(){
//		return status;
//	}
//
//	@Override
// 	public String toString(){
//		return
//			"QuestionModel{" +
//			"pending_count_curr_topic = '" + pendingCountCurrTopic + '\'' +
//			",data = '" + data + '\'' +
//			",overall_pending = '" + overallPending + '\'' +
//			",topic_id = '" + topicId + '\'' +
//			",message = '" + message + '\'' +
//			",status = '" + status + '\'' +
//			"}";
//		}
//}