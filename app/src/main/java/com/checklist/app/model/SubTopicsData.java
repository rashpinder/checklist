package com.checklist.app.model;

import com.google.gson.annotations.SerializedName;

public class SubTopicsData {

	@SerializedName("subtopic_name")
	private String subtopicName;

	@SerializedName("active")
	private boolean active;

	@SerializedName("id")
	private int id;

	@SerializedName("topic_id")
	private int topicId;

	@SerializedName("pending_question")
	private int pendingQuestion;

	public String getSubtopicName(){
		return subtopicName;
	}

	public boolean isActive(){
		return active;
	}

	public int getId(){
		return id;
	}

	public int getTopicId(){
		return topicId;
	}

	public int getPendingQuestion(){
		return pendingQuestion;
	}

	public void setSubtopicName(String subtopicName) {
		this.subtopicName = subtopicName;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public void setPendingQuestion(int pendingQuestion) {
		this.pendingQuestion = pendingQuestion;
	}

	@Override
	public String toString() {
		return "SubTopicsData{" +
				"subtopicName='" + subtopicName + '\'' +
				", active='" + active + '\'' +
				", id='" + id + '\'' +
				", topic_id='" + topicId + '\'' +
				", pendingQuestion='" + pendingQuestion + '\'' +
				'}';
	}
}