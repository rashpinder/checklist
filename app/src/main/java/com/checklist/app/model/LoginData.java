package com.checklist.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("profile_pic")
    @Expose
    private String profile_pic;

    @SerializedName("create_date")
    @Expose
    private String create_date;

    @SerializedName("disable")
    @Expose
    private String disable;

    @SerializedName("moblie_number")
    @Expose
    private String moblie_number;

    public LoginData() {
    }

    public String getMoblie_number() {
        return moblie_number;
    }

    public void setMoblie_number(String moblie_number) {
        this.moblie_number = moblie_number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", profile_pic='" + profile_pic + '\'' +
                ", create_date='" + create_date + '\'' +
                ", disable='" + disable + '\'' +
                ", moblie_number='" + moblie_number + '\'' +
                '}';
    }
}
